import { SUB_COUNTER, ADD_COUNTER } from "./actionTypes"

export const addCount = () => {
    return {
        type: ADD_COUNTER
    }
}

export const subCount = () => {
    return {
        type: SUB_COUNTER
    }
}