import { ADD_COUNTER, SUB_COUNTER } from "./actionTypes"

const reducerCounter = (state = 0, action) => {
    switch (action.type) {
        case ADD_COUNTER:
            return state + 1

        case SUB_COUNTER:
            return state - 1

        default:
            return state
    }
}

export default reducerCounter