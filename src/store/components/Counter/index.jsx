import { useDispatch, useSelector } from "react-redux";
import { addCount, subCount } from "../../modules/counter/actions";
import './style.css'

const Counter = () => {

    const dispatch = useDispatch()

    const counter = useSelector(state => state.counter)

    const handleClick = (operator) => {
        if (operator === "+") {
            dispatch(addCount())
        }
        if (operator === "-") {
            dispatch(subCount())
        }
    }

    return (
        <div className="container">
            <p className="number">{counter}</p>
            <div>
                <button onClick={e => handleClick(e.target.textContent)}>+</button>
                <button onClick={e => handleClick(e.target.textContent)}>-</button>
            </div>
        </div>
    )
}

export default Counter